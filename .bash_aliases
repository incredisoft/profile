alias ..='cd ..'
alias cd..='cd ..'
alias psx='ps aux | grep'
alias sstart='systemctl start'
alias sstatus='systemctl status'
alias grepa='grep -r'
alias aliases='cat ~/.bash_aliases'

alias rbenv_install='sudo apt install gcc && curl -fsSL https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer | bash'
