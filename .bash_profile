if [ -f ~/.bash_aliases ]; then
 . ~/.bash_aliases
fi

if [ -d ~/.rbenv/bin ] && ! [ `echo $PATH | grep 'rbenv' | wc -l` ]; then
 export PATH="$HOME/.rbenv/bin:$PATH"
 eval "$(rbenv init - bash)"
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# ADD SSH IDENTITY
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s`
    ssh-add
fi

# add prefix for the current git branch
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
if ! [ `echo $PS1 | grep "$(parse_git_branch)" | wc -l` ]; then
    export PS1="\[\033[32m\]\$(parse_git_branch)\[\033[00m\] $PS1"
fi
